package com.azerdemir.abrakkadabra.iot

import akka.actor.typed.{ActorRef, Behavior, PostStop}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import com.azerdemir.abrakkadabra.iot.DeviceManager.{
  Command,
  DeviceGroupTerminated,
  ReplyDeviceList,
  RequestDeviceList,
  RequestTrackDevice
}

object DeviceManager {

  sealed trait Command

  // TODO: This message is being replied by both DeviceManager and DeviceGroup. Find a better place to store it.
  final case class RequestTrackDevice(groupId: String, deviceId: String, replyTo: ActorRef[DeviceRegistered])
      extends DeviceManager.Command
      with DeviceGroup.Command

  final case class DeviceRegistered(device: ActorRef[Device.Command])

  final case class RequestDeviceList(requestId: Long, groupId: String, replyTo: ActorRef[ReplyDeviceList])
      extends DeviceManager.Command
      with DeviceGroup.Command

  final case class ReplyDeviceList(requestId: Long, ids: Set[String])

  final private case class DeviceGroupTerminated(groupId: String) extends DeviceManager.Command

  def apply(): Behavior[Command] =
    Behaviors.setup[Command] { context =>
      new DeviceManager(context).supervise(Map.empty)
    }

}

class DeviceManager private (context: ActorContext[Command]) {

  context.log.info("DeviceManager started")

  private def supervise(groupIdToActor: Map[String, ActorRef[DeviceGroup.Command]]): Behavior[Command] =
    Behaviors
      .receiveMessage[Command] {
        case trackMsg @ RequestTrackDevice(groupId, _, replyTo) =>
          groupIdToActor.get(groupId) match {
            case Some(ref) =>
              ref ! trackMsg
              Behaviors.same
            case None =>
              context.log.info("Creating device group actor for {}", groupId)
              val groupActor = context.spawn(DeviceGroup(groupId), "group-" + groupId)
              context.watchWith(groupActor, DeviceGroupTerminated(groupId))
              groupActor ! trackMsg
              supervise(groupIdToActor + (groupId -> groupActor))
          }

        case req @ RequestDeviceList(requestId, groupId, replyTo) =>
          groupIdToActor.get(groupId) match {
            case Some(ref) =>
              ref ! req
            case None =>
              replyTo ! ReplyDeviceList(requestId, Set.empty)
          }
          Behaviors.same

        case DeviceGroupTerminated(groupId) =>
          context.log.info("Device group actor for {} has been terminated", groupId)
          supervise(groupIdToActor - groupId)
      }
      .receiveSignal {
        case (_, PostStop) =>
          context.log.info("DeviceManager stopped")
          Behaviors.same
      }

}
