package com.azerdemir.abrakkadabra.iot

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{Behavior, PostStop}

object IotSupervisor {

  sealed trait SupervisorMessage
  case object StopYourself extends SupervisorMessage

  def apply(): Behavior[SupervisorMessage] =
    Behaviors.setup[SupervisorMessage] { context =>
      context.log.info("IoT Application started")

      Behaviors
        .receiveMessage[SupervisorMessage] {
          case StopYourself =>
            // no need to handle messages
            Behaviors.stopped
        }
        .receiveSignal {
          case (_, PostStop) =>
            context.log.info("IoT Application stopped")
            Behaviors.same
        }
    }

}
