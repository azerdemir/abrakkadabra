package com.azerdemir.abrakkadabra.iot

import akka.actor.typed.{ActorRef, Behavior, PostStop}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, LoggerOps}
import com.azerdemir.abrakkadabra.iot.DeviceGroup.{Command, DeviceTerminated}
import com.azerdemir.abrakkadabra.iot.DeviceManager.{
  DeviceRegistered,
  ReplyDeviceList,
  RequestDeviceList,
  RequestTrackDevice
}

object DeviceGroup {

  trait Command

  final private case class DeviceTerminated(device: ActorRef[Device.Command], groupId: String, deviceId: String)
      extends Command

  def apply(groupId: String): Behavior[Command] =
    Behaviors.setup[Command] { context =>
      new DeviceGroup(groupId, context).process(Map.empty)
    }

}

class DeviceGroup private (groupId: String, context: ActorContext[Command]) {

  context.log.info("DeviceGroup {} started", groupId)

  private def process(deviceIdToActor: Map[String, ActorRef[Device.Command]]): Behavior[Command] =
    Behaviors
      .receiveMessage[Command] {
        case trackMsg @ RequestTrackDevice(`groupId`, deviceId, replyTo) =>
          deviceIdToActor.get(deviceId) match {
            case Some(deviceActor) =>
              replyTo ! DeviceRegistered(deviceActor)
              Behaviors.same
            case None =>
              context.log.info("Creating device actor for {}", trackMsg.deviceId)
              val deviceActor = context.spawn(Device(groupId, deviceId), s"device-$deviceId")
              context.watchWith(deviceActor, DeviceTerminated(deviceActor, groupId, deviceId))
              replyTo ! DeviceRegistered(deviceActor)
              process(deviceIdToActor + (deviceId -> deviceActor))
          }

        case RequestTrackDevice(gId, _, _) =>
          context.log.warn2("Ignoring TrackDevice request for {}. This actor is responsible for {}.", gId, groupId)
          Behaviors.same

        case RequestDeviceList(requestId, gId, replyTo) =>
          if (gId == groupId) {
            replyTo ! ReplyDeviceList(requestId, deviceIdToActor.keySet)
            Behaviors.same
          } else
            Behaviors.unhandled

        case DeviceTerminated(_, _, deviceId) =>
          context.log.info("Device actor for {} has been terminated", deviceId)
          process(deviceIdToActor - deviceId)
      }
      .receiveSignal {
        case (_, PostStop) =>
          context.log.info("DeviceGroup {} stopped", groupId)
          Behaviors.same
      }

}
