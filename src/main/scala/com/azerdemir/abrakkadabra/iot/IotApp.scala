package com.azerdemir.abrakkadabra.iot

import akka.actor.typed.{ActorSystem, PostStop}
import com.azerdemir.abrakkadabra.iot.IotSupervisor.SupervisorMessage

object IotApp extends App {
  // Create ActorSystem and top level supervisor
  val actorSystem = ActorSystem[SupervisorMessage](IotSupervisor(), "iot-system")

  actorSystem ! IotSupervisor.StopYourself

  actorSystem.terminate()
}
