package com.azerdemir.abrakkadabra.iot

import akka.actor.typed.{ActorRef, Behavior, PostStop}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, LoggerOps}
import com.azerdemir.abrakkadabra.iot.Device._

object Device {

  sealed trait Command
  final case class ReadTemperature(requestId: Long, replyTo: ActorRef[RespondTemperature]) extends Command
  final case class RespondTemperature(requestId: Long, value: Option[Double])

  final case class RecordTemperature(requestId: Long, value: Double, replyTo: ActorRef[TemperatureRecorded])
      extends Command
  final case class TemperatureRecorded(requestId: Long)
  final case object Passivate extends Command

  def apply(groupId: String, deviceId: String): Behavior[Command] =
    Behaviors.setup[Command] { context =>
      new Device(groupId, deviceId, context).measureAndReport(None)
    }
}

class Device private (groupId: String, deviceId: String, context: ActorContext[Command]) {
  context.log.info2("Device actor {}-{} started", groupId, deviceId)

  private def measureAndReport(lastTemperatureReading: Option[Double]): Behavior[Command] =
    Behaviors
      .receiveMessage[Command] {
        case RecordTemperature(id, value, replyTo) =>
          context.log.info2("Recorded temperature reading {} with {}", value, id)
          replyTo ! TemperatureRecorded(id)
          measureAndReport(Some(value))

        case ReadTemperature(id, replyTo) =>
          replyTo ! RespondTemperature(id, lastTemperatureReading)
          Behaviors.same

        case Passivate =>
          Behaviors.stopped
      }
      .receiveSignal {
        case (_, PostStop) =>
          context.log.info2("Device actor {}-{} stopped", groupId, deviceId)
          Behaviors.same
      }
}
