ThisBuild / scalaVersion     := "2.13.2"
ThisBuild / version          := "0.1"
ThisBuild / organization     := "com.azerdemir"
ThisBuild / organizationName := "azerdemir"

lazy val akkaVersion = "2.6.8"

mainClass in (Compile, run) := Some("com.azerdemir.abrakkadabra.iot.IotApp")

lazy val root = (project in file("."))
  .settings(
    name := "abrakkadabra",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
      "ch.qos.logback"     % "logback-classic"  % "1.2.3",

      "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
      "org.scalatest"     %% "scalatest"                % "3.1.1"     % Test
    )
  )
